# frozen_string_literal: true

module ModelSearch
  class Base
    attr_reader :records, :params

    delegate :table_name, to: :klass

    class_attribute :default_sort, :default_sort_order, :sortables, :model_name,
                    instance_writer: false, instance_reader:

    def call(context = {})
      @records = context[:scope] || klass.all
      @params = context[:params] || {}
      @user = context[:user]

      unscoped_query
      query.order(Arel.sql(sort_criteria))
    end

    class << self
      def call(context = {})
        new.call(context)
      end

      def model_name(name = nil)
        return if name.blank?

        self.model_name = name.to_s
      end

      def default_sort(field = nil)
        return if field.blank?

        self.default_sort = field.to_s
      end

      def default_sort_order(order = nil)
        return if order.blank?

        self.default_sort_order = order.to_s
      end

      def sortable(*fields)
        self.sortables ||= {}

        options = fields.extract_options!

        fields.each do |field|
          sortables[field] = options[:condition] || "#{klass.table_name}.#{field}"
        end
      end

      def model_class
        model_name.present? ? model_name.camelize : self.name.split('::').first.singularize
      end

      def klass
        model_class.constantize
      end
    end

    protected

    def beginning_of_day(param)
      Time.zone.parse(param).beginning_of_day
    end

    def end_of_day(param)
      Time.zone.parse(param).end_of_day
    end

    private

    def klass
      @klass ||= self.model_class.constantize
    end

    # if unscoped=true is sent to the parameters, this will have to be added first in the chain.
    def unscoped_query
      return unless @params.key?(:unscoped) && @params[:unscoped] == 'true'

      @params.delete(:uncoped)

      @records = @records.unscoped
    end

    def query
      @params.each do |param, value|
        value = [nil] if value == []
        next unless respond_to?("#{param}_condition", true) && value.present?

        send("#{param}_condition", value)
      end

      default_conditions if respond_to?(:default_conditions, true)

      records
    end

    def sort_criteria
      "#{sort_column} #{sort_order} nulls last"
    end

    def sort_column
      sort = params[:sort] || default_sort.to_s
      sort = sort[1..] if sort.start_with?('-')
      sort_cond(sort)
    end

    def sort_cond(sort)
      return sort if sortables.nil?

      sortables[sort.to_sym] || default_sort
    end

    def sort_order
      return default_sort_order if params[:sort].blank?

      params[:sort].start_with?('-') ? :desc : :asc
    end

    def default_sort
      self.class.default_sort || "#{self.class.table_name}.created_at"
    end

    def default_sort_order
      self.class.default_sort_order || :desc
    end

    def sortables
      self.class.sortables
    end
  end
end
