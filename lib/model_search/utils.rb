# frozen_string_literal: true

module ModelSearch
  class Utils
    def self.root
      File.expand_path('../../', __dir__)
    end
  end
end
